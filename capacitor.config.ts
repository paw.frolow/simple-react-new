import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'ru.apptor.app',
  appName: 'simple-react-capacitor',
  webDir: 'build',
  server: {
    url: "http://192.168.31.64:3000"
  }
};

export default config;
