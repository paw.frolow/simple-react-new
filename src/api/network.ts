const base: string = 'http://example.com';

const api = {
  get: (route: string, params: RequestInit) =>
    request(route, {...params, method: 'GET'}),
  post: (route: string, params: RequestInit) =>
    request(route, {...params, method: 'POST'}),
  put: (route: string, params: RequestInit) =>
    request(route, {...params, method: 'PUT'}),
  delete: (route: string, params: RequestInit) =>
    request(route, {...params, method: 'DELETE'}),
};

const request = (route = '', params: RequestInit) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem('token');
    fetch(`${base}${route}`, {
      headers: new Headers({
        'Authorization': `Bearer ${token}`,
        ...params?.headers,
      }),
      ...params,
    })
      .then(responseCallback)
      .then((json) => {
        console.log(route, json);
        if (json.error) {
          reject(json);
        } else {
          resolve(json);
        }
      })
      .catch((error) => {
        console.error(route, error);
        if (error.status === 401 || error.status === 403) {
          //logout
        }
        reject(error);
      });
  });
};

const responseCallback = async (response: Response) => {
  console.log(response.status);
  const {status} = response;
  switch (status) {
    case 200:
    case 201:
    case 202:
    case 203:
    case 400:
    case 422:
      return response.json();
    case 204:
      return;
    case 401:
    case 403:
      throw {status, message: 'Неавторизован'};
    case 404:
      throw {status, message: 'Не удалось найти запрос'};
    default:
      throw {
        status,
        message: 'Произошла ошибка, пожалуйста попробуйте повторить позднее',
      };
  }
};

export default api;
