// App.jsx
import React, { useEffect } from 'react';
import {inject, observer} from 'mobx-react';
import {
  Page,
  NavRight,
  Navbar,
  Link,
  Toolbar,
  Tabs,
  Tab,
  Searchbar,
  List,
  ListItem,
  Toggle,
  BlockTitle,
  f7,
  ListInput,
} from 'framework7-react';
import { store } from '../stores';

export const MainPage = observer(() => {
  useEffect(() => {
    setInterval(() => {
      store.setLikesCount = store.getLikesCount + 1;
    }, 1000)
  }, []);
  return (
    <Page pageContent={false}>
      <Navbar title="CWBoss">
        <NavRight>
          <Link searchbarEnable=".searchbar-demo" iconOnly></Link>
        </NavRight>
        <Searchbar className="searchbar-demo" expandable placeholder="Поиск" />
      </Navbar>
      <Toolbar tabbar scrollable position={'top'}>
        <Link key={'points'} tabLink={`#tab-points`} tabLinkActive>
          Точки
        </Link>
        <Link key={'commas'} tabLink={`#tab-commas`}>
          Запятые
        </Link>
        <Link key={'points-commas'} tabLink={`#tab-points-commas`}>
          Точки с запятой
        </Link>
      </Toolbar>

      <Tabs>
        <Tab
          key={'points'}
          id={`tab-points`}
          className="page-content"
          tabActive
        >
          <BlockTitle medium>Likes count {store.getLikesCount}</BlockTitle>
          <List>
            <ListItem title="Acura" after="Acura"></ListItem>
            <ListItem link="#" title="BMW"></ListItem>
            <ListItem link="#" title="Cadillac "></ListItem>
            <ListItem link="#" title="Audi"></ListItem>
            <ListItem title="Chevrolet ">
              <Toggle slot="after"></Toggle>
            </ListItem>
          </List>
          <BlockTitle medium>Examples</BlockTitle>
          <List mediaList>
            <ListItem
              link="#"
              title="Yellow Submarine"
              after="$15"
              subtitle="Beatles"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
            >
              <img
                slot="media"
                src="https://cdn.framework7.io/placeholder/people-160x160-1.jpg"
                width="80"
                alt=""
              />
            </ListItem>
            <ListItem
              link="#"
              title="Don't Stop Me Now"
              after="$22"
              subtitle="Queen"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
            >
              <img
                slot="media"
                src="https://cdn.framework7.io/placeholder/people-160x160-2.jpg"
                width="80"
                alt=""
              />
            </ListItem>
            <ListItem
              link="#"
              title="Billie Jean"
              after="$16"
              subtitle="Michael Jackson"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
            >
              <img
                slot="media"
                src="https://cdn.framework7.io/placeholder/people-160x160-3.jpg"
                width="80"
                alt=""
              />
            </ListItem>
          </List>
          <BlockTitle medium>More Examples</BlockTitle>
          <List>
            <ListItem
              checkbox
              title="Books"
              name="demo-checkbox"
              defaultChecked
            />
            <ListItem checkbox title="Movies" name="demo-checkbox" />
            <ListItem
              radio
              radioIcon="start"
              title="Books"
              value="Books"
              name="demo-radio-start"
              defaultChecked
            ></ListItem>
            <ListItem
              radio
              radioIcon="start"
              title="Movies"
              value="Movies"
              name="demo-radio-start"
            ></ListItem>
            <ListItem
              radio
              radioIcon="end"
              title="Books"
              value="Books"
              name="demo-radio-end"
              defaultChecked
            ></ListItem>
            <ListItem
              radio
              radioIcon="end"
              title="Movies"
              value="Movies"
              name="demo-radio-end"
            ></ListItem>
          </List>
          <List mediaList>
            <ListItem
              checkbox
              defaultChecked
              name="demo-media-checkbox"
              title="Facebook"
              after="17:14"
              subtitle="New messages from John Doe"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
            />
            <ListItem
              checkbox
              name="demo-media-checkbox"
              title="John Doe (via Twitter)"
              after="17:11"
              subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
            />
          </List>
        </Tab>
        <Tab key={'commas'} id={`tab-commas`} className="page-content">
          2
        </Tab>
        <Tab
          key={'points-commas'}
          id={`tab-points-commas`}
          className="page-content"
        >
          3
        </Tab>
      </Tabs>
    </Page>
  );
})
