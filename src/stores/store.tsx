import { makeAutoObservable } from 'mobx'

class Store {
    constructor() {
        makeAutoObservable(this);
    }

    likesCount: number = 1;

    get getLikesCount() {
        return this.likesCount;
    }

    set setLikesCount(value: number) {
        this.likesCount = value;
    }
}

export default new Store();