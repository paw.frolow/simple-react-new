# Simple-react-v2

## Сборка

Сборка статического проекта (папка www)

```bash
npm run build
```

### Cordova

Сборка под обе платформы

```bash
npm run build:cordova
```

Сборка под конкретную платформу

```bash
npm run build:cordova-ios
```

```bash
npm run build:cordova-android
```

### Capacitor

Сборка под обе платформы

```bash
npm run build:cap
```

Сборка под конкретную платформу

```bash
npm run build:cap-ios
```

```bash
npm run build:cap-android
```

## Иконки

При создании сборке платформ cordova / capacitor должны нарезаться сплеш-скрины и иконки, для этого нужно поставить глобально

поставить глобально cordova-res

```bash
sudo npm install cordova-res -g
```

или resgen

```bash
sudo npm install cordova-res-generator -g
```

в папке resources необходимо чтобы были изображения без альфа каналов:
icon.png (1024 x 1024), splash.png (2732 x 2732), папка android, содержащая icon-foreground.png и icon-background.png размерами 432 x 432

### cordova-res

```bash
npm run res
```

или

```bash
cordova-res --skip-config --copy
```

## Решение проблем

### xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

1. Install Xcode
2. Run command

```bash
sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
```

### HTTP запросы не проходят в android

android:usesCleartextTraffic="true" атрибут в тег <application ... > в android manifest

### Unable to lookup service com.apple.CoreSimulator.host_support: 0x3

1. Please go to Xcode->Window->Device and Simulators
2. Select Simulator option from segment
3. Remove simulator and add again

### Blank page after running build on create-react-app

Add "homepage": ".", in your package.json then build again

1. Run npm run eject
2. Edit file webpack.config in the config folder
3. Find path with "static/" or "static/js/" or "static/css/" in the file and delete such path
4. Build your project again
